### Problem Statement 11

Given an N-sided simple polygon whose vertices are lattice points on the X-Y coordinate system (i.e. points whose co-ordinates are integers), find the total number of lattice points lying within the polygon (not including the boundary!)


**Hint:** Use Pick’s Theorem.


**Input format:**

The first line of input consists of an integer T which consists of the number of test cases. Then T lines follow with the input given in the following format:


N x<sub>1</sub> y<sub>1</sub> x<sub>2</sub> y<sub>2</sub> … x<sub>N-1</sub> y<sub>N-1</sub>


Where (x<sub>i</sub>,y<sub>i</sub>) are all the vertices of the polygon taken in order.


**Output format:**

For each test case, print out the total number of lattice points P lying within the polygon:

P

**Constraints:**


1<=T<=10000

1<=N<=10000


**Sample Input:**
```
1
3 0 0 0 5 5 0
```
**Sample Output:**
```
6
```
**Note:** When generating test cases, ensure that all polygons are simple and non-intersecting!

