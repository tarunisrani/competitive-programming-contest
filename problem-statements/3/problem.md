### Problem Statement 3

In a planet a year has a y number of days. There are no leap years in this planet. Aliens in this planet generally have a happy life. They congregate in groups of k, such that the probability of at least two aliens in this group sharing the same birthday is greater than or equal to j%. 

Myna is an expert in this alien civilization but is not very good at programming. She wants your help in computing the value k. Help her!!


**Constraints:**

y, k and j are all integers. N is the number of test cases.

1<=N<=1000

1 <= y <= 10000

0<=j<=100


**Input Format:**

The first line of input consists of N which is the number of test cases. Then N lines follow with the input format as follows:

y j


**Output Format:**

For each test case, find the minimum value k.


**Sample Input:**

```
1
365 50
```
**Sample Output:**

```
23
```